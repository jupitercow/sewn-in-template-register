<?php
/*
Plugin Name: Sewn In Template Register
Plugin URI: http://bitbucket.org/jupitercow/acf-sewn-in-template-register
Description: Replace registration form using ACF. Moves everything to a page template.
Version: 1.0.0
Author: Jake Snyder
Author URI: http://Jupitercow.com/
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

------------------------------------------------------------------------
Copyright 2014 Jupitercow, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

if (! class_exists('customize_register') ) :

add_action( 'plugins_loaded', array('customize_register', 'plugins_loaded') );

add_action( 'init', array('customize_register', 'init') );

class customize_register
{
	/**
	 * Class prefix
	 *
	 * @since 	1.0.0
	 * @var 	string
	 */
	public static $prefix = __CLASS__;

	/**
	 * Current version of plugin
	 *
	 * @since 	1.0.0
	 * @var 	string
	 */
	public static $version = '1.0.0';

	/**
	 * Settings
	 *
	 * @since 	1.0.0
	 * @var 	string
	 */
	public static $settings = array();

	/**
	 * Holds the error/update action messages
	 *
	 * @since 	1.0.0
	 * @var 	string
	 */
	public static $messages;

	/**
	 * Initialize the Class
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function init()
	{
		if (! self::test_requirements() ) return;

		add_filter( self::$prefix . '/settings/get_path', array(__CLASS__, 'helpers_get_path'), 1 );
		add_filter( self::$prefix . '/settings/get_dir',  array(__CLASS__, 'helpers_get_dir'), 1 );

		// Set up settings
		self::$settings = array(
			'path'     => apply_filters( self::$prefix . '/settings/get_path', __FILE__ ),
			'dir'      => apply_filters( self::$prefix . '/settings/get_dir',  __FILE__ ),
			'pages'    => array(
				'register' => array(
					'page_name'  => 'register',
					'page_title' => 'Register'
				),
				'profile' => array(
					'page_name'  => 'profile',
					'page_title' => 'Profile'
				)
			)
		);

		self::$messages = array(
			'registered' => array(
				'key'     => 'action',
				'value'   => 'registered',
				'message' => __("Successfully registered!", self::$prefix),
				'args'    => 'fade=true&page=' . self::$settings['pages']['profile']['page_name'] . ',' . self::$settings['pages']['register']['page_name']
			),
			'profile' => array(
				'key'     => 'action',
				'value'   => 'profile',
				'message' => __("Profile updated", self::$prefix),
				'args'    => 'fade=true&page=' . self::$settings['pages']['profile']['page_name']
			),
			'exists' => array(
				'key'     => 'action',
				'value'   => 'exists',
				'message' => __("That email address already exists, follow the links below to get access", self::$prefix),
				'args'    => 'page=' . self::$settings['pages']['register']['page_name']
			),
			'invalid' => array(
				'key'     => 'action',
				'value'   => 'invalid',
				'message' => __("Please enter a valid email address", self::$prefix),
				'args'    => 'page=' . self::$settings['pages']['register']['page_name']
			)
		);
		self::$messages = apply_filters( 'acf/' . self::$prefix . '/add_messages', self::$messages );

		// Add a basic title/content interface for default use
		add_action( 'wp',                         array(__CLASS__, 'register_field_groups') );

		// Redirect pages if the user is logged in or not
		add_action( 'template_redirect',          array(__CLASS__, 'template_redirect') );

		// Add form to Registration Page
		add_filter( 'the_content',                array(__CLASS__, 'add_form_to_page') );

		// Add a fake post for profile and register pages if they don't already exist
		add_filter( 'the_posts',                  array(__CLASS__, 'add_post') );

		// Load any scripts and styles
		add_action( 'wp_enqueue_scripts',         array(__CLASS__, 'enqueue_scripts') );

		// Test if the email exists already
		add_action( 'wp_ajax_' . self::$prefix . '_test_email',        array(__CLASS__, 'ajax_test_email') );
		add_action( 'wp_ajax_nopriv_' . self::$prefix . '_test_email', array(__CLASS__, 'ajax_test_email') );

		// When the plugin is activated or deativated
		register_activation_hook( __FILE__,       array(__CLASS__, 'register_activation_hook') );
		register_deactivation_hook( __FILE__,     array(__CLASS__, 'register_deactivation_hook') );
	}

	/**
	 * Make sure that any neccessary dependancies exist
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	bool True if everything exists
	 */
	public static function test_requirements()
	{
		// Look for ACF
		if (! class_exists('Acf') ) return false;
		return true;
	}

	/**
	 * On plugins_loaded test if we can use frontend_notifications
	 *
	 * @author  Jake Snyder
	 * @since	0.2.1
	 * @return	void
	 */
	public static function plugins_loaded()
	{
		// Have the login plugin use frontend notifictions plugin
		if ( apply_filters( 'acf/' . self::$prefix . '/use_frontend_notifications', true ) )
		{
			if ( class_exists('frontend_notifications') )
			{
				add_filter( 'frontend_notifications/queries', array(__CLASS__, 'add_notification_messages') );
			}
			else
			{
				add_filter( self::$prefix . '/use_frontend_notifications', '__return_false' );
			}
		}
	}

	/**
	 * Add this plugin's notification messages to the frontend_notifications plugin.
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	array $queries The modified queries for the frontend_notification plugin
	 */
	public static function add_notification_messages( $queries )
	{
		$queries = array_merge($queries, self::$messages);
		return $queries;
	}
    
	/**
	 * See if not register post exists and add it dynamically if not
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	object $posts Modified $posts with the new register post
	 */
	public static function add_post( $posts )
	{
		global $wp, $wp_query;

		// Check if the requested page matches our target, and no posts have been retrieved
		if ( (! $posts || 1 < count($posts)) && array_key_exists(strtolower($wp->request), self::$settings['pages']) )
		{
			// Add the fake post
			$posts   = array();
			$posts[] = self::create_post( strtolower($wp->request) );

			$wp_query->is_page     = true;
			$wp_query->is_singular = true;
			$wp_query->is_home     = false;
			$wp_query->is_archive  = false;
			$wp_query->is_category = false;
			//Longer permalink structures may not match the fake post slug and cause a 404 error so we catch the error here
			unset($wp_query->query["error"]);
			$wp_query->query_vars["error"]="";
			$wp_query->is_404=false;
		}
		return $posts;
	}
    
	/**
	 * Create a dynamic post on-the-fly for the register page.
	 *
	 * source: http://scott.sherrillmix.com/blog/blogger/creating-a-better-fake-post-with-a-wordpress-plugin/
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	object $post Dynamically created post
	 */
	public static function create_post( $type )
	{
		// Create a fake post.
		$post = new stdClass();
		$post->ID                    = 1;
		$post->post_author           = 1;
		$post->post_date             = current_time('mysql');
		$post->post_date_gmt         = current_time('mysql', 1);
		$post->post_content          = '';
		$post->post_title            = self::$settings['pages'][$type]['page_title'];
		$post->post_excerpt          = '';
		$post->post_status           = 'publish';
		$post->comment_status        = 'closed';
		$post->ping_status           = 'closed';
		$post->post_password         = '';
		$post->post_name             = self::$settings['pages'][$type]['page_name'];
		$post->to_ping               = '';
		$post->pinged                = '';
		$post->post_modified         = current_time('mysql');
		$post->post_modified_gmt     = current_time('mysql', 1);
		$post->post_content_filtered = '';
		$post->post_parent           = 0;
		$post->guid                  = home_url('/' . self::$settings['pages'][$type]['page_name'] . '/');
		$post->menu_order            = 0;
		$post->post_type             = 'page';
		$post->post_mime_type        = '';
		$post->comment_count         = 0;
		$post->filter                = 'raw';
		return $post;   
	}

	/**
	 * Adds a form to the login page, this can be turned off using the filter: 'customize_register/add_form'
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	string $content The post content for login page with the login form a
	 */
	public static function add_form_to_page( $content )
	{
		if ( is_main_query() && in_the_loop() && apply_filters( 'acf/' . self::$prefix . '/add_form', true ) )
		{
			$messages = $footer = '';
			$args = false;
			if (! empty($_GET['action']) )
			{
				$action = $_GET['action'];
				if (! apply_filters( 'acf/' . self::$prefix . '/use_frontend_notifications', true ) && apply_filters( 'acf/' . self::$prefix . '/show_messages', true ) )
					$messages = (! empty(self::$messages[$action]['message']) ) ? '<p class="' . self::$prefix . '_messages">' . self::$messages[$action]['message'] . '</p>' : '';
			}

			if ( is_page(self::$settings['pages']['register']['page_name']) )
			{
				$args = array(
					'field_groups' => apply_filters( 'acf/' . self::$prefix . '/register/field_groups', array() ),
				);
				ob_start(); ?>
				<div class="hidden" style="display:none; visibility:hidden;">
				<form name="loginform" id="loginform" action="<?php echo site_url('wp-login.php?action=lostpassword', 'login_post'); ?>" method="post">
					<input type="text" name="user_login" id="user_login" class="input" value="" size="20" />
					<input type="hidden" name="redirect_to" value="<?php echo add_query_arg('action', 'recovered', home_url('/login/')); ?>" />
				</form>
				</div>
				<?php
				$footer = ob_get_clean();
			}
			elseif ( is_page(self::$settings['pages']['profile']['page_name']) )
			{
				add_filter( 'acf/load_value/name=username', array(__CLASS__, 'load_value_user_login'), 10, 3 );
				add_filter( 'acf/load_value/name=email',    array(__CLASS__, 'load_value_user_email'), 10, 3 );

				$args = array(
					'field_groups' => apply_filters( 'acf/' . self::$prefix . '/profile/field_groups', array() ),
					'form_type' => 'profile'
				);
			}

			$content = $messages . $content . ($args ? self::form( $args ) : '') . $footer;
		}

		return $content;
	}

	public static function load_value_user_login( $value, $post_id, $field )
	{
		$current_user = wp_get_current_user();
		return $current_user->user_login;
	}
	public static function load_value_user_email( $value, $post_id, $field )
	{
		$current_user = wp_get_current_user();
		return $current_user->user_email;
	}

	/**
	 * Create the register/profile form.
	 *
	 * Works almost exactly like "acf_form"
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	string $content The post content for login page with the login form a
	 */
	public static function form( $options )
	{
		// defaults
		$defaults = array(
			'field_groups' => array(),
			'submit_value' => false,
			'form_type'    => 'register'
		);

		// merge defaults with options
		$options = array_merge($defaults, $options);

		// Add default Submit Value
		if (! $options['submit_value'] )
		{
			$options['submit_value'] = ( 'profile' == $options['form_type'] ) ? __("Update") : __("Register");
		}

		// Set up the post ID
		if ( is_user_logged_in() && 'profile' == $options['form_type'] )
		{
			$current_user = wp_get_current_user();
			$options['post_id'] = 'user_' . $current_user->ID;
		}
		else
		{
			$options['post_id'] = 'new_user';
		}

		// Add the default field groups to anything the user adds
		$options['field_groups'] = array_merge( array( 'acf_register' ), $options['field_groups'] );

		// Add the return
		$action                  = ( 'profile' == $options['form_type'] ) ? 'profile' : 'registered';
		$options['return']       = add_query_arg( 'action', $action, get_permalink() );

		// The actual form
		ob_start();
		acf_form( $options );
		return ob_get_clean();
	}

	/**
	 * Redirect the pages depending on if the user is logged in or not.
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	string $content The post content for login page with the login form a
	 */
	public static function template_redirect()
	{
		if ( array_key_exists(get_query_var('pagename'), self::$settings['pages']) )
		{
			$redirect = false;
			if ( 'register' == get_query_var('pagename') && is_user_logged_in() )
			{
				$redirect = apply_filters( 'acf/' . self::$prefix . '/redirect/register', home_url('/profile/') );
			}
			if ( 'profile' == get_query_var('pagename') && ! is_user_logged_in())
			{
				$redirect = apply_filters( 'acf/' . self::$prefix . '/redirect/profile', wp_login_url() );
			}
			if ( $redirect ) wp_redirect( $redirect );

			// Load the acf form head
			acf_form_head();
		}
	}

	/**
	 * AJAX wrapper for the test_email function
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function ajax_test_email()
	{
		if ( empty($_POST['email']) ) return;

		echo self::test_email( $_POST['email'] );
		die;
	}

	/**
	 * Test the email to see if it exists
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function test_email( $user_email )
	{
		if (! is_email( $user_email ) )
		{
			return 'invalid';
		}
		elseif ( email_exists( $user_email ) )
		{
			return 'exists';
		}
	}

	/**
	 * Enqueue scripts and styles
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function enqueue_scripts()
	{
		// register scripts
		wp_register_script( self::$prefix, self::$settings['dir'] . 'js/scripts.js', array( 'jquery' ), self::$version );
		$args = array(
			'url'      => admin_url( 'admin-ajax.php' ),
			'action'   => self::$prefix . '_test_email',
			'prefix'   => self::$prefix,
			'version'  => self::$version,
			'spinner'  => admin_url( 'images/spinner.gif' ),
			'messages' => self::$messages,
			'links'    => array(
				'login'    => array(
					'href'     => add_query_arg( 'email', '[email]', wp_login_url() ),
					'text'     => __("Log In")
				),
				'recover'  => array(
					'href'     => '#javascript_required',
					'text'     => __("Recover your password", self::$prefix)
				)
			)
		);
		wp_localize_script( self::$prefix, self::$prefix, $args );

		wp_enqueue_script( array(
			self::$prefix
		) );

		// register styles
		wp_register_style( self::$prefix, self::$settings['dir'] . 'css/style.css', array(), self::$version );

		wp_enqueue_style( array(
			self::$prefix
		) );
	}

	/**
	 * Runs when the plugin is Activated
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function register_activation_hook()
	{
		flush_rewrite_rules();
	}

	/**
	 * Runs when the plugin is deactivated
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function register_deactivation_hook()
	{
		flush_rewrite_rules();
	}

	/**
	 * Get the plugin path
	 *
	 * Calculates the path (works for plugin / theme folders). These functions are from Elliot Condon's ACF plugin.
	 *
	 * @since	1.0.0
	 * @return	void
	 */
	public static function helpers_get_path( $file )
	{
	   return trailingslashit( dirname($file) );
	}

	/**
	 * Get the plugin directory
	 *
	 * Calculates the directory (works for plugin / theme folders). These functions are from Elliot Condon's ACF plugin.
	 *
	 * @since	1.0.0
	 * @return	void
	 */
	public static function helpers_get_dir( $file )
	{
        $dir = trailingslashit( dirname($file) );
        $count = 0;

        // sanitize for Win32 installs
        $dir = str_replace('\\' ,'/', $dir);

        // if file is in plugins folder
        $wp_plugin_dir = str_replace('\\' ,'/', WP_PLUGIN_DIR); 
        $dir = str_replace($wp_plugin_dir, plugins_url(), $dir, $count);

        if ( $count < 1 )
        {
	       // if file is in wp-content folder
	       $wp_content_dir = str_replace('\\' ,'/', WP_CONTENT_DIR); 
	       $dir = str_replace($wp_content_dir, content_url(), $dir, $count);
        }

        if ( $count < 1 )
        {
	       // if file is in ??? folder
	       $wp_dir = str_replace('\\' ,'/', ABSPATH); 
	       $dir = str_replace($wp_dir, site_url('/'), $dir);
        }

        return $dir;
    }

	/**
	 * Add a basic registration form
	 *
	 * @author  Jake Snyder
	 * @since	1.0.0
	 * @return	void
	 */
	public static function register_field_groups()
	{
		if ( function_exists("register_field_group") )
		{
			$args = array(
				'key' => 'acf_register',
				'title' => apply_filters( 'acf/' . self::$prefix . '/group/title', "Register" ),
				'fields' => array (),
				'location' => array (
					array (
						array (
							'param' => 'ef_user',
							'operator' => '!=',
							'value' => 'all',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'no_box',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => -10,
			);

			if ( apply_filters( 'acf/' . self::$prefix . '/username/add', true ) )
			{
				$args['fields'][] = array (
					'key'           => 'field_52b75fe28969e_login',
					'label'         => apply_filters( 'acf/' . self::$prefix . '/username/title', 'Username' ),
					'name'          => apply_filters( 'acf/' . self::$prefix . '/username/name', 'username' ),
					'type'          => apply_filters( 'acf/' . self::$prefix . '/username/type', 'text' ),
					'required'      => apply_filters( 'acf/' . self::$prefix . '/username/required', 1 ),
					'default_value' => '',
					'placeholder'   => '',
					'prepend'       => '',
					'append'        => '',
					'formatting'    => 'html',
					'maxlength'     => '',
				);
			}

			$args['fields'][] = array (
				'key'           => 'field_52b76078896a1_email',
				'label'         => apply_filters( 'acf/' . self::$prefix . '/email/title', 'Email Address' ),
				'name'          => apply_filters( 'acf/' . self::$prefix . '/email/name', 'email' ),
				'type'          => apply_filters( 'acf/' . self::$prefix . '/email/type', 'email' ),
				'required'      => apply_filters( 'acf/' . self::$prefix . '/email/required', 1 ),
				'default_value' => '',
				'placeholder'   => '',
				'prepend'       => '',
				'append'        => '',
			);

			if ( apply_filters( 'acf/' . self::$prefix . '/name/add', true ) )
			{
				$args['fields'][] = array (
					'key'           => 'field_52b75fe28969e_first_name',
					'label'         => apply_filters( 'acf/' . self::$prefix . '/first_name/title', 'First Name' ),
					'name'          => apply_filters( 'acf/' . self::$prefix . '/first_name/name', 'first_name' ),
					'type'          => apply_filters( 'acf/' . self::$prefix . '/first_name/type', 'text' ),
					'required'      => apply_filters( 'acf/' . self::$prefix . '/first_name/required', 1 ),
					'default_value' => '',
					'placeholder'   => '',
					'prepend'       => '',
					'append'        => '',
					'formatting'    => 'html',
					'maxlength'     => '',
				);
				$args['fields'][] = array (
					'key'           => 'field_52b7603a8969f_last_name',
					'label'         => apply_filters( 'acf/' . self::$prefix . '/last_name/title', 'Last Name' ),
					'name'          => apply_filters( 'acf/' . self::$prefix . '/last_name/name', 'last_name' ),
					'type'          => apply_filters( 'acf/' . self::$prefix . '/last_name/type', 'text' ),
					'required'      => apply_filters( 'acf/' . self::$prefix . '/last_name/required', 1 ),
					'default_value' => '',
					'placeholder'   => '',
					'prepend'       => '',
					'append'        => '',
					'formatting'    => 'html',
					'maxlength'     => '',
				);
			}

			if ( apply_filters( 'acf/' . self::$prefix . '/password/add', true ) )
			{
				$args['fields'][] = array (
					'key'         => 'field_52b760ea896a2_pass1',
					'label'       => apply_filters( 'acf/' . self::$prefix . '/pass1/title', 'Password' ),
					'name'        => apply_filters( 'acf/' . self::$prefix . '/pass1/name', 'pass1' ),
					'type'        => apply_filters( 'acf/' . self::$prefix . '/pass1/type', 'password' ),
					'required'    => apply_filters( 'acf/' . self::$prefix . '/pass1/required', 1 ),
					'placeholder' => '',
					'prepend'     => '',
					'append'      => '',
				);
				$args['fields'][] = array (
					'key'         => 'field_52b7610b896a3_pass2',
					'label'       => apply_filters( 'acf/' . self::$prefix . '/pass2/title', 'Password (re-enter)' ),
					'name'        => apply_filters( 'acf/' . self::$prefix . '/pass2/name', 'pass2' ),
					'type'        => apply_filters( 'acf/' . self::$prefix . '/pass2/type', 'password' ),
					'required'    => apply_filters( 'acf/' . self::$prefix . '/pass2/required', 1 ),
					'placeholder' => '',
					'prepend'     => '',
					'append'      => '',
				);
			}

			register_field_group( $args );
		}
	}
}

endif;